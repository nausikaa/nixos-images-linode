# NixOS images for Linode

Scripts, instructions, and configs to partially automate the process of generating and deploying NixOS images for Linode.
Based on manual instructions here: https://www.linode.com/docs/tools-reference/custom-kernels-distros/install-nixos-on-linode/

### Dependencies

linode-cli https://github.com/linode/linode-cli
Run `linode-cli configure` before trying to run this script.

### Usage

#### Create Image

The only user input that may be required before running create-image.sh is to configure a version of NixOS to install by setting the `Version` variable at the top of the script. It is currently set to what is now the latest version: `Version="20.03"`

It is recommended to first read the Linode documentation for how to Install and Configure NixOS on a Linode: https://www.linode.com/docs/tools-reference/custom-kernels-distros/install-nixos-on-linode/
With a few exceptions, the steps of the script are nearly identical to those outlined in the documentation.

To generate a NixOS image on your Linode account, run the script `bash create-image.sh`
The first half of the script is automated, the second half generates instructions that includes commands that must be run manually. If it goes smoothly, the whole process should take around 20 minutes.

#### Deploy Node

The only user input required before running create-linode.sh is to configure the `host` and `user` variables at the top of the script.

The `host` variable will be used as the label for the new node. If you decide to use the script for configuration of NixOS, it will also be the name of the file from /etc/nixos/hosts that will be symlinked to in place of configuration.nix.

The `user` variable is intended to be your Linode account name. If you have an SSH public key associated with your account, this key will be added to the authorized_keys file of the root user of the new node when it is created.

To create a new NixOS node on your account, run the script `bash create-linode.sh`. There is one verification in the middle about whether or not to continue with configuration of NixOS or to stop after deploying the base image. Other than this, the script requires no interaction. It it does smoothly, deploying a simple configuration should only take a few minutes.   
