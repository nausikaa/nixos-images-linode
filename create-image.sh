#!/bin/bash

Version="20.09"
Timestamp=`date +%Y-%m-%d-%H-%M`
ImageName="nixos$Version-$Timestamp"

red=`tput setaf 5`
off=`tput sgr0`

echo " "
echo "Creating a temporary node to generate a new NixOS image."
linode-cli linodes create --type g6-nanode-1 --label NewNixOSImageNode && sleep 25 &&
nodeid=$(linode-cli linodes list --text --format 'id,label' | grep NewNixOSImageNode | cut -f1) && sleep 15 &&
echo "If this script seems to fail, run this command to clean up the node before retrying: ${red}linode-cli linodes delete $nodeid${off}"
echo " " && sleep 5 &&


echo "Creating disks for the installer image, swap, and root."
echo " "
linode-cli linodes disk-create $nodeid --label InstallerImage --size 1024 --filesystem ext4 && sleep 10 &&
installerimageid=$(linode-cli linodes disks-list $nodeid --text --format 'id,label' | grep InstallerImage | cut -f1) && sleep 15 &&
linode-cli linodes disks-list $nodeid | grep -C1 -E "label|InstallerImage" &&
echo " "
linode-cli linodes disk-create $nodeid --label Swap --size 512 --filesystem swap && sleep 15 &&
swapid=$(linode-cli linodes disks-list $nodeid --text --format 'id,label' | grep Swap | cut -f1) && sleep 5 &&
linode-cli linodes disks-list $nodeid | grep -C1 -E "label|Swap" | grep -v ext4 &&
echo " "
linode-cli linodes disk-create $nodeid --label Root --size 24064 --filesystem ext4 && sleep 15 &&
rootid=$(linode-cli linodes disks-list $nodeid --text --format 'id,label' | grep Root | cut -f1) && sleep 5 &&
linode-cli linodes disks-list $nodeid | grep -C1 -E "label|Root" | grep -Ev "swap|--" &&
echo " "


echo "Creating two configuration profiles, one to boot the installer and the other to boot NixOS."
echo " "
linode-cli linodes config-create $nodeid \
  --label "InstallerBoot" \
  --kernel linode/direct-disk \
  --devices.sda.disk_id $rootid \
  --devices.sdb.disk_id $swapid \
  --devices.sdc.disk_id $installerimageid \
  --root_device /dev/sdc && sleep 5 &&
installerbootid=$(linode-cli linodes configs-list $nodeid --text --format 'id,label' | grep InstallerBoot | cut -f1) && sleep 5 &&
echo " "
linode-cli linodes config-create $nodeid \
  --label "NixOSBoot" \
  --kernel linode/grub2 \
  --devices.sda.disk_id $rootid \
  --devices.sdb.disk_id $swapid \
  --root_device /dev/sda && sleep 5 &&
nixosbootid=$(linode-cli linodes configs-list $nodeid --text --format 'id,label' | grep NixOSBoot | cut -f1) && sleep 5 &&
echo " "


echo "Booting the node into rescue mode with a blank disk for the installer image mounted as /dev/sda."
echo " "
linode-cli linodes rescue $nodeid --devices.sda.disk_id $installerimageid && sleep 15 &&
linode-cli linodes list | grep -C1 -E "label|NewNixOSImageNode" | grep -Ev "online|offline" && sleep 5 &&
echo " "


echo "Now connect to Weblish at this page and wait for the boot to finish:"
echo "${red}https://cloud.linode.com/linodes/$nodeid/lish/weblish${off}"
echo "Once in rescue mode (aka Finnix) on Weblish, paste this command:"
echo "${red} update-ca-certificates && iso=https://channels.nixos.org/nixos-$Version/latest-nixos-minimal-x86_64-linux.iso ; curl -L \$iso | tee >(dd of=/dev/sda) | sha256sum | tr -d '-' && checkfile=\$(curl -w \"%{url_effective}\n\" -I -L -s -S \$iso -o /dev/null | sed 's/$/.sha256/') && curl -s \$checkfile | cut -d' ' -f1 && echo \"Verify that the two checksum values above are the same before proceeding.\"${off}"
echo "When the download is finished and the checksum verified, leave Weblish open and return here."
echo " "
read -p "Press enter to continue."
echo " "


echo "Rebooting the node into the new NixOS installer image..."
linode-cli linodes reboot $nodeid --config_id $installerbootid && sleep 20 &&
echo " "
echo "Return to Weblish on the browser and switch to the Glish tab to wait for Finnix to shut down and then for the NixOS installer to boot."
echo " " && sleep 2 &&
echo "At the NixOS installer prompt in Glish, type out this command to download the install-nixos.sh script from DistroSync (Glish does not reliably copy-paste):"
echo "${red}curl -LO script.distrosync.com/install-nixos.sh${off}"
# Target of shortened URL: https://gitlab.com/distrosync/nixos-images-linode/-/raw/master/install-nixos.sh
echo " "
read -p "After the download is complete, press enter and continue."
echo " "

echo "Inspect the contents of the script: ${red}cat install-nixos.sh${off} and then run it: ${red}bash install-nixos.sh${off}"
echo " "
rootpass=$(tr -dc 'A-Za-z0-9' < /dev/urandom | head -c 12) &&
echo "The NixOS install will ask you for a root password. Type out this one: ${red}$rootpass${off}"
read -p "After NixOS is installed, press enter to continue."
echo " "


echo "Booting into the new NixOS system..."
linode-cli linodes reboot $nodeid --config_id $nixosbootid && sleep 30 &&
echo " "
ip=$(linode-cli linodes list --text --format "id,ipv4"| grep $nodeid | cut -f2) && sleep 3 &&

echo "When the boot is finished, login using Weblish with user ${red}root${off}"
echo "Paste the password above using ${red}shift-insert${off} instead of ctrl-v."
echo " "
read -p "Press enter to continue."
echo " "


echo "Then run this command: ${red}ssh-keygen -lf /etc/ssh/ssh_host_ed25519_key.pub${off} and save the host key fingerprint for when you first login with SSH."
echo " "
echo "This is also a good time to make any customizations you want included in the image."
echo " "
read -p "Press enter to continue."
echo " "

echo "Open a new local terminal and login: ${red}ssh root@$ip${off}"
echo " "
echo "Verify that the host key fingerprints match, enter ${red}yes${off}, paste the root password to complete the login, and then ${red}exit${off} to logout."
echo " "
read -p "Press enter to continue."
echo " "


echo "Getting the host key fingerprint from the current user's known_hosts file, to be added to the image description field."
hostkeyfp=$(grep $ip ~/.ssh/known_hosts | tail -n1 | cut -d' ' -f3) && sleep 3 &&
echo " "

echo "Enter the root password one last time to complete garbage collection of /nix/store and and flushing logs before creating image:"
ssh root@$ip "nix-collect-garbage -d && journalctl --flush && journalctl --rotate && journalctl --vacuum-time=1s" && sleep 5 &&
echo " "
echo "Shutting down NixOS..."
linode-cli linodes shutdown $nodeid && sleep 30 &&
echo " "

echo "Checking how many images are already stored on your account."
echo " "
imagecount=$(linode-cli images list | grep private | wc -l) && sleep 5 &&
if [ "${imagecount}" -eq 3 ]
then
  echo "You have reached the normal account limit of 3 stored images."
  echo " "
  read -p "Delete at least one of them here ${red}https://cloud.linode.com/images${off} and then press enter to continue."
  echo " "
fi

echo "Creating the new image, called $ImageName"
echo "This can take a few minutes."
linode-cli images create --label $ImageName --description $hostkeyfp --disk_id $rootid > /dev/null && sleep 15 &&
imagesize=$(linode-cli images list --text --format 'label,size' | grep $ImageName | cut -f2) &&
echo " "
count=1 && sleep 60 && echo "1 minute"
while [ $imagesize -gt 1950 ]
do
  imagesize=$(linode-cli images list --text --format 'label,size' | grep $ImageName | cut -f2) && sleep 60 && 
  count=$(( $count + 1 )) &&
  echo "$count minutes"
done
echo " "
echo "Done!"
echo " "


echo "Deleting the node that was used to create the image."
linode-cli linodes delete $nodeid &&
echo " "
