#!/bin/bash

echo "Setting up the install environment."

# Mount the installer image
sudo mount /dev/sda /mnt &&

# Enable the swap disk
sudo swapon /dev/sdb &&

# Generate a starter configuration
sudo nixos-generate-config --root /mnt &&

echo "Downloading base NixOS configs from a DistroSync repository."

cd /mnt/etc/nixos &&
sudo mv configuration.nix configuration.nix.bak &&
sudo mv hardware-configuration.nix hardware-configuration.nix.bak &&
sudo curl -O https://gitlab.com/distrosync/nixos-images-linode/-/raw/master/configuration.nix &&
sudo curl -O https://gitlab.com/distrosync/nixos-images-linode/-/raw/master/hardware-configuration.nix &&

echo "Installing NixOS, please wait to enter a root password."

sudo nixos-install

echo "NixOS installed."
